# Thesis

# Name
Michael Schrauben thesis scripts and off-target report.

# Description
This project displays the code used to generate data and plots throughout my thesis. The scripts are split by data chapter and are not exhaustive of all the analysis attempted during my PhD, but represent those analyses included in the final document. Additionally, the raw output from the WGE tool is available in the Off-target report folder.

# Support
For further information please email ms1011@exeter.ac.uk
